#!/usr/bin/env python3
# by @Jamesits 2016-08-23
# Great thanks to @m13253

def dec_unicode_decode(s):
  return ''.join((bytes([x&255, x>>8]).decode('utf-16-le') for x in map(int, s.split('#')[1:])))

def dec_unicode_encode(s):
  return '#' + '#'.join([str(int(x, 16)) for x in str(s.encode("unicode-escape"))[2:-1].split("\\\\u")[1:]])

# examples
if __name__ == "__main__":
  print(enc("计算工具"))
  print(dec("#35745#31639#24037#20855"))